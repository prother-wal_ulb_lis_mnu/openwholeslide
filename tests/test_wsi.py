import os
import unittest

import numpy as np

from src.openwholeslide import FloatVector, IntVector
from src.openwholeslide.wsi import WholeSlide

PROJECT_LEVEL = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEST_SAMPLE_TIFF = os.path.join(PROJECT_LEVEL, "samples", "sample_histo.tif")


class TestWSI(unittest.TestCase):

    wsi = WholeSlide(TEST_SAMPLE_TIFF)

    def test_wsi_read(self):
        assert self.wsi.dimensions.x == 2220 and self.wsi.dimensions.y == 2967 and self.wsi.dimensions.z is None
        assert self.wsi.mag == 20

    def test_wsi_mpp_mag(self):
        assert self.wsi.mpp == self.wsi.um_per_pixel_at_mag(self.wsi.mag)
        assert self.wsi.um_per_pixel_at_mag(self.wsi.mag / 2) == self.wsi.mpp.scale(2)

    def test_wsi_region(self):
        assert self.wsi.read_full(1).px_dimensions == self.wsi.dimensions.scale(1 / self.wsi.mag).round()

        region = self.wsi.read_region(
            location=FloatVector.from_yx((0.25, 0.25)), magnification=10, dimensions=FloatVector.from_yx((0.5, 0.25))
        )

        assert region.px_dimensions.x == 277
        assert region.px_dimensions.y == 741

        assert region.as_ndarray.shape == region.px_dimensions.yx + (3,)
        # sitk -> size = 1 for the "z-dimension", no channel dimension
        assert region.as_sitk.GetSize() == region.px_dimensions.xy + (1,)
        assert region.as_pil.size == region.px_dimensions.xy
        assert region.mpp(factor=1e-3).xy == self.wsi.mm_per_pixel_at_mag(10).xy
        assert region.mpp(factor=1).xy == self.wsi.um_per_pixel_at_mag(10).xy

    def test_region_padding(self):
        region = self.wsi.read_region(
            location=FloatVector.from_yx((0.25, 0.25)), magnification=10, dimensions=FloatVector.from_yx((0.5, 0.25))
        )
        region_nd_original = region.as_ndarray
        region.pad(region.px_dimensions + IntVector.from_yx((50, 50)), center=True)

        assert region.as_ndarray[:25, :25].sum() == 0
        assert np.allclose(region.as_ndarray[25:-25, 25:-25], region_nd_original)
