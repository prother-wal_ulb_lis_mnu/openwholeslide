# Changelog

## v0.0.14 (17/07/2023)

- Add project urls in pyproject.toml.

## v0.0.12 (17/07/2023)

- Add Usage section in the README.

## v0.0.11 (17/07/2023)

- Fix missing dependency.

## v0.0.6 (13/07/2023)

- First release of `openwholeslide`!